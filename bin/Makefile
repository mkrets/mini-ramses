#############################################################################
# If you have problems with this makefile, contact Romain.Teyssier@gmail.com
#############################################################################
# Compilation time parameters
NVECTOR = 32
NDIM = 3
NHILBERT = 1
NPRE = 8
NVAR = 5
NENER = 0
SOLVER = hydro
PATCH = 
EXEC = ramses
#############################################################################
GITBRANCH = $(shell git rev-parse --abbrev-ref HEAD)
GITHASH = $(shell git log --pretty=format:'%H' -n 1)
GITREPO = $(shell git config --get remote.origin.url)
BUILDDATE = $(shell date +"%D-%T")
DEFINES = -DNVECTOR=$(NVECTOR) -DNDIM=$(NDIM) -DNPRE=$(NPRE) -DNENER=$(NENER) -DNVAR=$(NVAR) \
	  -DNHILBERT=$(NHILBERT) -DHYDRO -DGRAV
#############################################################################
# Fortran compiler options and directives

# --- No MPI, gfortran -------------------------------
F90 = gfortran -O3 -frecord-marker=4 -fbacktrace -ffree-line-length-none -g
FFLAGS = -x f95-cpp-input $(DEFINES) -DWITHOUTMPI

# --- No MPI, tau ----------------------------------
#F90 = tau_f90.sh -optKeepFiles -optPreProcess -optCPPOpts=$(DEFINES) -DWITHOUTMPI

# --- No MPI, pgf90 ----------------------------------
#F90 = pgf90
#FFLAGS = -Mpreprocess $(DEFINES) -DWITHOUTMPI

# --- No MPI, xlf ------------------------------------
#F90 = xlf
#FFLAGS = -WF,-DNDIM=$(NDIM),-DNPRE=$(NPRE),-DNVAR=$(NVAR),-DSOLVER$(SOLVER),-DWITHOUTMPI -qfree=f90 -qsuffix=f=f90 -qsuffix=cpp=f90

# --- No MPI, f90 ------------------------------------
#F90 = f90
#FFLAGS = -O3 -g -cpp $(DEFINES) -DWITHOUTMPI

# --- No MPI, ifort ----------------------------------
#F90 = ifort
#FFLAGS = -O3 -g -traceback -cpp $(DEFINES) -DWITHOUTMPI

# --- MPI, gfortran syntax ------------------------------
#F90 = mpif90 -frecord-marker=4 -O3 -ffree-line-length-none -g -fbacktrace 
#FFLAGS = -x f95-cpp-input $(DEFINES)

# --- MPI, gfortran DEBUG syntax ------------------------------ 
#F90 = mpif90 -frecord-marker=4 -ffree-line-length-none -fbacktrace -g -O -fbounds-check -Wall
#FFLAGS = -x f95-cpp-input -ffpe-trap=zero,underflow,overflow,invalid -finit-real=nan  $(DEFINES)    

# --- MPI, pgf90 syntax ------------------------------
#F90 = mpif90 -O3
#FFLAGS = -Mpreprocess $(DEFINES)

# --- MPI, ifort syntax ------------------------------
#F90 = mpif90
#FFLAGS = -cpp -check uninit -O3 -g -traceback -fpe0 -ftrapuv -check bounds  $(DEFINES) -DNOSYSTEM
#F90 = mpif90 -f90=ifort
#FFLAGS = -cpp -g -O2 -debug inline-debug-info -shared-intel -shared-libgcc $(DEFINES) -DNOSYSTEM
#FFLAGS = -cpp -g -O2 -debug inline-debug-info -shared-intel -shared-libgcc -traceback -fpe0 -ftrapuv -check all $(DEFINES) -DNOSYSTEM
# -DWITHOUTMPI
# -check all -ftrapuv

# --- MPI, ifort syntax, additional checks -----------
#F90 = mpif90 -f90=ifort
#FFLAGS = -warn all -O0 -g -traceback -fpe0 -ftrapuv -check bounds -cpp $(DEFINES) -DNOSYSTEM
#FFLAGS = -cpp -fast -g  $(DEFINES) -DNOSYSTEM
#FFLAGS = -cpp  $(DEFINES) -DNOSYSTEM    

# --- MPI, ifort syntax ------------------------------
#F90 = ftn
#FFLAGS = -xAVX -g -traceback -fpp -fast $(DEFINES) -DNOSYSTEM #-DRT 

# --- MPI, ifort syntax, additional checks -----------
#F90 = ftn
#FFLAGS = -O3 -g -traceback -fpe0 -ftrapuv -cpp $(DEFINES) -DNOSYSTEM #-DRT

C = ifort
CFLAGS = -g -O3


#############################################################################
MOD = mod
#############################################################################
# MPI librairies
LIBMPI = 
#LIBMPI = -lfmpi -lmpi -lelan

# --- CUDA libraries, for Titane ---
LIBCUDA = -L/opt/cuda/lib  -lm -lcuda -lcudart

LIBS = $(LIBMPI)
#############################################################################
# Sources directories are searched in this exact order
VPATH = $(PATCH):../$(SOLVER):../aton:../hydro:../pm:../poisson:../amr
#############################################################################

# Modules
MODOBJ = amr_parameters.o hydro_parameters.o hash.o pm_parameters.o pm_commons.o hilbert.o domain_hilbert.o \
         hydro_commons.o amr_commons.o poisson_parameters.o poisson_commons.o \
	 mdl_commons.o ramses_commons.o call_back.o cache_commons.o gadgetreadfile.o write_makefile.o write_patch.o write_gitinfo.o

# AMR objects
AMROBJ = timer.o read_params.o init_amr.o init_time.o init_refine_basegrid.o init_refine_adaptive.o init_refine_restart.o \
	 adaptive_loop.o update_time.o title.o units.o refine_utils.o nbors_utils.o flag_utils.o smooth.o \
         amr_step.o output_amr.o load_balance.o movie.o task_manager.o

# Particle-Mesh objects
PMOBJ = newdt_fine.o init_part.o input_part.o input_part_restart.o input_part_ascii.o input_part_grafic.o rho_fine.o move_fine.o rho_ana.o output_part.o

# Poisson solver objects
POISSONOBJ = output_poisson.o force_fine.o grav_ana.o phi_fine_cg.o upload.o interpol_phi.o poisson_flag.o multigrid_fine_commons.o multigrid_fine_coarse.o

# Hydro objects
HYDROOBJ = init_hydro.o init_flow_fine.o input_hydro_condinit.o input_hydro_grafic.o interpol_hydro.o \
	   condinit.o hydro_flag.o godunov_utils.o write_screen.o courant_fine.o \
	   godunov_fine.o umuscl.o uplmde.o output_hydro.o synchro_hydro_fine.o cooling_fine.o

# All objects
AMRLIB = $(AMROBJ) $(HYDROOBJ) $(PMOBJ) $(POISSONOBJ)

# ATON objects
ATON_MODOBJ = timing.o radiation_commons.o rad_step.o
ATON_OBJ = observe.o init_radiation.o rad_init.o rad_boundary.o rad_stars.o rad_backup.o ../aton/atonlib/libaton.a
#############################################################################
ramses:	$(MODOBJ) $(AMRLIB) ramses.o
	$(F90) $(MODOBJ) $(AMRLIB) ramses.o -o $(EXEC)$(NDIM)d $(LIBS)
	rm write_makefile.f90
	rm write_patch.f90

amr2map:   amr_parameters.o amr_commons.o hydro_parameters.o hydro_commons.o ../utils/f90/amr2map.f90
	 $(F90) amr_parameters.o amr_commons.o hydro_parameters.o hydro_commons.o ../utils/f90/amr2map.f90 -o amr2map

ramses_aton: $(MODOBJ) $(ATON_MODOBJ) $(AMRLIB) $(ATON_OBJ) ramses.o
	$(F90) $(MODOBJ) $(ATON_MODOBJ) $(AMRLIB) $(ATON_OBJ) ramses.o -o $(EXEC)$(NDIM)d $(LIBS) $(LIBCUDA)
	rm write_makefile.f90
	rm write_patch.f90
#############################################################################
write_gitinfo.o: FORCE
	$(F90) $(FFLAGS) -DPATCH=\'$(PATCH)\' -DGITBRANCH=\'$(GITBRANCH)\' -DGITHASH=\'"$(GITHASH)"\' \
-DGITREPO=\'$(GITREPO)\' -DBUILDDATE=\'"$(BUILDDATE)"\' -c ../amr/write_gitinfo.f90 -o $@		
write_makefile.o: FORCE
	../utils/scripts/cr_write_makefile.sh $(MAKEFILE_LIST)
	$(F90) $(FFLAGS) -c write_makefile.f90 -o $@
write_patch.o: FORCE
	../utils/scripts/cr_write_patch.sh $(PATCH)
	$(F90) $(FFLAGS) -c write_patch.f90 -o $@
%.o:%.f90
	$(F90) $(FFLAGS) -c $^ -o $@
%.o:%.c
	$(C) $(CFLAGS) -c $^ -o $@

FORCE:
#############################################################################
clean :
	rm *.o *.$(MOD)
#############################################################################
